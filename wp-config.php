
<?php
define( 'WP_CACHE', true );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pcldb2' );

/** MySQL database username */
define( 'DB_USER', 'prestigedb_user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'prestijdb1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i4k8dsdb4e0ikhzabhtapyhxv9ty3wwv51gucee5wthbh3lffbkynzqgad2y5uxc' );
define( 'SECURE_AUTH_KEY',  '0vik86gti3bnyaefbf63jamgbruu4cah8u9z2qtesdzlzje58hlalu2xp4bzqogh' );
define( 'LOGGED_IN_KEY',    'ma1xmk8euprcovbrcclrldbhxc1c5hfxpudvek08c8k8xkung8wsjbwc64rpxske' );
define( 'NONCE_KEY',        'znyasjlvumobsh2g3u13rx4tc0wcbigebuqgxd9kveznmsmuu76hlfgsc3jhnbrp' );
define( 'AUTH_SALT',        'aohacicgiuri9axklcalizvt2qasfcdxepibque2wjhrtctw2atlf1yeqev3f5f5' );
define( 'SECURE_AUTH_SALT', 'phw6ev6wbdcrohmorxuvmujincujlsar0qoezflq4bevcuxqveyfifhlngo6hxg6' );
define( 'LOGGED_IN_SALT',   'mdkvx429otskb6o2xycnoinujo8v2dmjarq5ijxewkjllc5w8mnfoog2ttuv6nj1' );
define( 'NONCE_SALT',       'agc6ezdzkba1wy9ml6vvv77d0ifecs2babafctmvydfqd9jxkipkigbnqoa1yiix' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpxp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
